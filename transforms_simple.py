from transforms import rotate_right, mirror, blur, grayscale
from images import read_img, write_img
import sys

def transform():
    if len(sys.argv) != 3:
        print("Uso: python script.py <nombre_imagen> <funcion>")
        sys.exit(1)

    image_filename = sys.argv[1]
    function_name = sys.argv[2]

    pixels = read_img(image_filename)

    if function_name == "rotate_right":
        pixels = rotate_right(pixels)
    elif function_name == "mirror":
        pixels = mirror(pixels)
    elif function_name == "blur":
        pixels = blur(pixels)  # Corregir la llamada a blur()
    elif function_name == "grayscale":
        pixels = grayscale(pixels)
    else:
        print("Función no soportada: {}".format(function_name))
        sys.exit(1)

    output_filename = "{}_trans.{}".format(*image_filename.split("."))
    write_img(pixels, output_filename)

def main():
    transform()

if __name__ == '__main__':
    main()