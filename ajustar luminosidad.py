import sys
def ajustar_luminosidad(imagen, factor):
    width, height = imagen.size
    pix = imagen.load()

    for i in range(width):
        for j in range(height):
            r, g, b = imagen.getpixel((i, j))
            tr = int(r * factor)
            tg = int(g * factor)
            tb = int(b * factor)

            pix[i, j] = (min(tr, 255), min(tg, 255), min(tb, 255))

def ajustar_contraste(imagen, factor):
    width, height = imagen.size
    pix = imagen.load()

    for i in range(width):
        for j in range(height):
            r, g, b = imagen.getpixel((i, j))
            tr = int(factor * (r - 128) + 128)
            tg = int(factor * (g - 128) + 128)
            tb = int(factor * (b - 128) + 128)

            pix[i, j] = (min(max(tr, 0), 255), min(max(tg, 0), 255), min(max(tb, 0), 255))

