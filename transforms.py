## Puede haber una o más funciones definidas, que utilizará main()
import sys
def change_colors(image: list[list[tuple[int, int, int]]],to_change: tuple[int, int, int],to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
#pixeles es una lista de listas de tuplas (r,g,b)
    for column in image:
        for pixel in range(len(column)):
            if column[pixel] == to_change:
              column[pixel] = to_change_to
    return image

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
#Rota la imagen 90 grados hacia la derecha
    rotated_image = []
    for column in zip(*image):  # Recorre la imagen transpuesta
        rotated_image.append(list(reversed(column)))  # Invierte cada fila
    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
#Devuelve la imagen epejada horizontalmente
    image_mirror = []
    for row in image[::-1]:
        image_mirror.append(row)
    return image_mirror

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    # Cambia cada pixel (r,g,b) a (r+increment, g+increment, b+increment) % 256
    image_rotated = []
    for column in image:
        # Cambia cada pixel (r,g,b) a (r+increment, g+increment, b+increment) % 256
        row = []
        for pixel in column:
            r, g, b = pixel
            r = (r + increment) % 256
            g = (g + increment) % 256
            b = (b + increment) % 256
            row.append((r, g, b))
        image_rotated.append(row)
    return image_rotated

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
# Aplica un filtro de blur a la imagen promediando cada pixel
    image_blurred = []
    for row in range(len(image)):
        new_row = []
        for col in range(len(image[row])):
            r = g = b = 0
            pixel_count = 0
            for i in range(-1, 2):
                for j in range(-1, 2):
                    r_neighbor = g_neighbor = b_neighbor = 0
                    neighbor_row = row + i
                    neighbor_col = col + j
                    if 0 <= neighbor_row < len(image) and 0 <= neighbor_col < len(image[row]):
                        r_neighbor, g_neighbor, b_neighbor = image[neighbor_row][neighbor_col]
                        r += r_neighbor
                        g += g_neighbor
                        b += b_neighbor
                        pixel_count += 1
            r = int(r/pixel_count)
            g = int(g/pixel_count)
            b = int(b/pixel_count)
            new_row.append((r,g,b))
        image_blurred.append(new_row)
    return image_blurred

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
# Desplaza la imagen el numero de pixeles indicado en el eje horizontal y vertical
    image_shifted = []
    for row in range(len(image)):
        new_row = []
        for col in range(len(image[row])):
            new_row_index = (row + vertical) % len(image)
            new_col_index = (col + horizontal) % len(image[row])
            new_row.append(image[new_row_index][new_col_index])
        image_shifted.append(new_row)
    return image_shifted
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int, image_cropped) -> list[list[tuple[int, int, int]]]:
# Recorta la imagen a partir de las coordenadas (x,y) con un ancho y alto dado
    image_croppped = []
    for row in range(y, y+height):
        new_row = []
        for col in range(x, x+width):
            new_row.append(image[row][col])
        image_cropped.append(new_row)
    return image_cropped
def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
# Convierte la imagen a escala de grises
    image_grayscale = []
    for row in image:
        grayscale_row = []
        for pixel in row:
            r, g, b = pixel
            gray = int((r + g + b) / 3)
            grayscale_row.append((gray, gray, gray))
        image_grayscale.append(grayscale_row)
    return image_grayscale

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
# Aplica un filtro a la imagen
    image_filtered = []
    for row in image:
        new_row = []
    for pixel in row: # Aplica el filtro a cada canal r, g, b
        new_r = int(pixel[0] * r)
        new_g = int(pixel[1] * g)
        new_b = int(pixel[2] * b)
        new_row.append((new_r, new_g, new_b))
    image_filtered.append(new_row)
    return image_filtered