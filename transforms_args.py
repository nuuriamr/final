from transforms import change_colors, rotate_colors, filter, shift, crop
from images import read_img, write_img
import sys

def transforms():
    if len(sys.argv) < 4:
        print("Usage: python script.py <image_name> <function> <args>")
        sys.exit(1)

    try:
        pixels = read_img(sys.argv[1])
    except Exception as e:
        print(f"Error opening the image: {e}")
        sys.exit(1)

    function = sys.argv[2].lower()  # Convertir a minúsculas para ser insensible a mayúsculas
    args = sys.argv[3:]

    try:
        if function == "change_colors":
            pixels_old = (int(args[0]), int(args[1]), int(args[2]))
            pixels_new = (int(args[0]), int(args[4]), int(args[5]))
            pixels = change_colors(pixels, pixels_old, pixels_new)
        elif function == "rotate_colors":
            pixels = rotate_colors(pixels, int(args[0]))
        elif function == "filter":
            pixels = filter(pixels, float(args[0]), float(args[1]), float(args[2]))
        elif function == "shift":
            pixels = shift(pixels, int(args[0]), int(args[1]))
        elif function == "crop":
            pixels = crop(pixels, int(args[0]), int(args[1]), int(args[2]), int(args[3]))
        else:
            print("Function not supported")
            sys.exit(1)
    except Exception as e:
        print(f"Error applying function {function}: {e}")
        sys.exit(1)

    output_filename = f"{sys.argv[1].split('.')[0]}_trans.{sys.argv[1].split('.')[1]}"
    write_img(pixels, output_filename)
    print(f"Transformations applied successfully. Image saved to '{output_filename}'")

def main():
    transforms()

if __name__ == '__main__':
    main()